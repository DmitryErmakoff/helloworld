package main

import (
	"bufio"
	"os"
)

func main() {
	wr := bufio.NewWriter(os.Stdin)
	defer wr.Flush()
	wr.WriteString("Hello world!")
}
